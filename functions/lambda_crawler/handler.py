import os

import boto3
from lxml import html
from urllib.request import Request, urlopen

# Create an SNS client
sns = boto3.client('sns')
xpath_to_find_data = "//form/button"


def get_product_availability(product_url: str)-> bool:
    response = urlopen(Request(product_url))
    tree1 = html.fromstring(response.read())
    product_availability = tree1.xpath(xpath_to_find_data)[0].text_content().strip()
    print(f'{product_url} {product_availability}')
    if product_availability != 'Sold Out':
        return True
    return False


def handler(event, context):
    products = [
        'https://shop.spacex.com/collections/mens/products/men-s-starman-t-shirt?variant=29801109356623',
        'https://shop.spacex.com/collections/mens/products/men-s-starman-t-shirt?variant=29801109389391',
        'https://shop.spacex.com/collections/accessories/products/dragon-tumbler'
    ]
    if all(map(get_product_availability, products)):
        # Publish a simple message to the specified SNS topic
        products_string = '\n'.join(products)
        response = sns.publish(
            TopicArn=os.environ['snsArn'],
            Message=f"Your space is products are ready to BUY!! \n {products_string}",
        )
        print("Shut up and take my money!", response)
    else:
        print('Sad')
